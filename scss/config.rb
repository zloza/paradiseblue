relative_assets = true
css_dir = "../css"
sass_dir = "../scss"
images_dir = "../img"

output_style = :nested
line_comments = false

preferred_syntac = :scss